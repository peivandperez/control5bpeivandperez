--------------------------------------------------------
-- Archivo creado  - jueves-junio-25-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table GOLES
--------------------------------------------------------

  CREATE TABLE "GOLES" ("JUG_RUT" NUMBER, "GOL_CANTIDAD" NUMBER);
--------------------------------------------------------
--  DDL for Table JUGADOR
--------------------------------------------------------

  CREATE TABLE "JUGADOR" ("JUG_RUT" NUMBER, "JUG_NOMBRE" VARCHAR2(20), "EQU_COD" NUMBER);
--------------------------------------------------------
--  DDL for Table EQUIPO
--------------------------------------------------------

  CREATE TABLE "EQUIPO" ("EQU_CODIGO" NUMBER, "EQU_NOMBRE" VARCHAR2(40));
REM INSERTING into GOLES
SET DEFINE OFF;
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('1','3');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('2','6');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('3','2');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('4','4');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('5','6');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('6','9');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('7','1');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('8','2');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('9','2');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('10','6');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('11','1');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('12','2');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('13','7');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('14','3');
Insert into GOLES (JUG_RUT,GOL_CANTIDAD) values ('15','5');
REM INSERTING into JUGADOR
SET DEFINE OFF;
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('1','RAMOS','1');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('2','BUTRAGUENO','1');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('3','ZAMORANO','1');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('4','STOICHKOV','2');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('5','LAUDRUP','2');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('6','ROMARIO','2');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('7','GUERRERO','3');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('8','LLORENTE','3');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('9','ETXEBERRIA','3');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('10','KIKO','4');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('11','GODIN','4');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('12','GRIEZMANN','4');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('13','MENDIETA','5');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('14','KEMPES','5');
Insert into JUGADOR (JUG_RUT,JUG_NOMBRE,EQU_COD) values ('15','VILLA','5');
REM INSERTING into EQUIPO
SET DEFINE OFF;
Insert into EQUIPO (EQU_CODIGO,EQU_NOMBRE) values ('1','REAL MADRID');
Insert into EQUIPO (EQU_CODIGO,EQU_NOMBRE) values ('2','BARCELONA');
Insert into EQUIPO (EQU_CODIGO,EQU_NOMBRE) values ('3','ATHLETIC BILBAO');
Insert into EQUIPO (EQU_CODIGO,EQU_NOMBRE) values ('4','ATLETICO MADRID');
Insert into EQUIPO (EQU_CODIGO,EQU_NOMBRE) values ('5','VALENCIA');

/*drop table goles;
drop table jugador;
drop table equipo;*/