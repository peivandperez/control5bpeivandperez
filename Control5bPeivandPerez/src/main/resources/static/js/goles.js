function GolesController(option){
	$("#msg").hide();
	$("#msg").removeClass("alert-success").addClass("alert-danger");

	switch(option){
	
	case "listAll":
		$.ajax({
			type : "post",
			url : "/goles/listAll",
			success : function(res){
				$("#goleadoresTable").bootstrapTable('load',res);
				$("#goleadoresTable tbody").on('click','tr', function(){
					$("#jugNombre").val($(this).find("td:eq(0)").text());
					$("#equNombre").val($(this).find("td:eq(1)").text());
					$("#golCantidad").val($(this).find("td:eq(2)").text());
					$("#jug_rut").val($(this).find("td:eq(3)").text());
					$("#myModalListAll").click()		
				});
				$("#myModalListAll").modal({show:true})
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error en búsqueda de notas");
			}
		});
		break;
		
	case "golesEquipo":
		$.ajax({
			type : "post",
			url : "/goles/golesEquipo",
			success : function(res){
				$("#golesEquipoTable").bootstrapTable('load',res);	
				$("#myModalGolesEquipo").modal({show:true})
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error en búsqueda de goles");
			}
		});
		break;
	
	
	case "update":
		var json = 
	{
			'golCantidad' : $("#golCantidad").val(),
			'jug_rut' : $("#jug_rut").val()
			
	}
		var postData= JSON.stringify(json);
		
		$.ajax({
			type : "post",
			url : "/goles/update",
			data: postData,
			contentType : "application/json; charset=utf-8",
			success : function(res){
				if (res== 1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Cantidad de goles ha sido actualizado correctamente")
				}else{
					$("#msg").show();
					$("#msg").html("Error al acutalizar goles")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error al actualizar goles");
			}
		});
		break;
	
	
	default:
			$("#msg").show();
			$("#msg").html("Opción Inválida");
	}
}