package cl.peivandPerez.Control5B.Model.DAO;

import java.util.List;

import cl.peivandPerez.Control5B.Model.DTO.GolesDTO;

public interface GolesDAO {
	public List<GolesDTO> listAll();
	public List<GolesDTO> getGolesEquipo();
	public int update(GolesDTO golesDTO);
}
