package cl.peivandPerez.Control5B.Model.DTO;

public class GolesDTO {

	private int jug_rut;
	private int golCantidad;
	private String jugNombre;
	private String equNombre;
	private int equ_codigo;
	private int total_goles;
	
	
	public int getTotal_goles() {
		return total_goles;
	}
	public void setTotal_goles(int total_goles) {
		this.total_goles = total_goles;
	}
	public int getJug_rut() {
		return jug_rut;
	}
	public void setJug_rut(int jug_rut) {
		this.jug_rut = jug_rut;
	}
	public int getGolCantidad() {
		return golCantidad;
	}
	public void setGolCantidad(int golCantidad) {
		this.golCantidad = golCantidad;
	}
	public String getJugNombre() {
		return jugNombre;
	}
	public void setJugNombre(String jugNombre) {
		this.jugNombre = jugNombre;
	}
	public String getEquNombre() {
		return equNombre;
	}
	public void setEquNombre(String equNombre) {
		this.equNombre = equNombre;
	}
	public int getEqu_codigo() {
		return equ_codigo;
	}
	public void setEqu_cod(int equ_codigo) {
		this.equ_codigo = equ_codigo;
	}
	
	public GolesDTO() {
	}
	
	

	
	
}
