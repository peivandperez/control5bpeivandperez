package cl.peivandPerez.Control5B.Model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import cl.peivandPerez.Control5B.Model.DTO.GolesDTO;
@Repository
@Transactional
public class GolesDAOImpl implements GolesDAO {
	private String selectAllSQL="SELECT j.jug_nombre, e.equ_nombre, g.gol_cantidad, j.jug_rut, e.equ_codigo "
								+"FROM jugador j, equipo e, goles g "
								+"WHERE j.jug_rut=g.jug_rut AND e.equ_codigo=j.equ_cod "
								+"ORDER BY g.gol_cantidad DESC";
	
	
	private String selectGolesEquipo="SELECT e.equ_nombre, sum(g.gol_cantidad) as total_goles "
									+"FROM jugador j, equipo e, goles g "
									+"WHERE j.jug_rut=g.jug_rut AND e.equ_codigo=j.equ_cod "
									+"GROUP BY e.equ_nombre "
									+"ORDER BY total_goles DESC";
	
	private String updateGoles= "UPDATE goles set gol_cantidad= :golCantidad "
							 	+"WHERE jug_rut= :jug_rut";
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public List<GolesDTO> listAll() {
		
		List<GolesDTO> list=namedParameterJdbcTemplate.query(selectAllSQL,BeanPropertyRowMapper.newInstance(GolesDTO.class));
		return list;
	}

	@Override
	public List<GolesDTO> getGolesEquipo() {
		
		
		List<GolesDTO> listGolesEquipo=namedParameterJdbcTemplate.query(selectGolesEquipo,BeanPropertyRowMapper.newInstance(GolesDTO.class));
		return listGolesEquipo;
	}

	@Override
	public int update(GolesDTO golesDTO) {
			int rows =0;
			
			MapSqlParameterSource params =new MapSqlParameterSource();
			
			params.addValue("golCantidad",golesDTO.getGolCantidad(),Types.INTEGER);
			params.addValue("jug_rut",golesDTO.getJug_rut(),Types.INTEGER);
			 
			try {
		
			
			
				rows = namedParameterJdbcTemplate.update(updateGoles, params);
				
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			return rows;
		}
	}


