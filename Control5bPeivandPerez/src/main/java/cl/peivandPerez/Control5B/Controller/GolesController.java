package cl.peivandPerez.Control5B.Controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import cl.peivandPerez.Control5B.Model.DTO.GolesDTO;
import cl.peivandPerez.Control5B.Service.GolesService;

@RestController
@RequestMapping(value="/goles")
public class GolesController {

	@Autowired
	GolesService golesService;
	
	@RequestMapping(value="/listAll")
	public List<GolesDTO> ajaxListAll(HttpServletRequest req, HttpServletResponse res) {
		List<GolesDTO> goles = golesService.listAll();
		return goles;
	}
	
	@RequestMapping(value="/golesEquipo")
	public List<GolesDTO> ajaxGolesEquipo(HttpServletRequest req, HttpServletResponse res) {
		List<GolesDTO> goles = golesService.getGolesEquipo();
		return goles;
	}
	
	@RequestMapping(value="/update")
	public int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			GolesDTO golesDTO = new Gson().fromJson(requestData, GolesDTO.class);
			rows = golesService.update(golesDTO);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
}