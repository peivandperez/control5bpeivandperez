package cl.peivandPerez.Control5B;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Control5bPeivandPerezApplication {

	public static void main(String[] args) {
		SpringApplication.run(Control5bPeivandPerezApplication.class, args);
	}

}
