package cl.peivandPerez.Control5B.Service;

import java.util.List;
import org.springframework.stereotype.Service;
import cl.peivandPerez.Control5B.Model.DTO.GolesDTO;

public interface GolesService {
	public List<GolesDTO> listAll();
	public List<GolesDTO> getGolesEquipo();
	public int update(GolesDTO golesDTO);
}
