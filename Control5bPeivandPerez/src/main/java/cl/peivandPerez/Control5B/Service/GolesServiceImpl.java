package cl.peivandPerez.Control5B.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.peivandPerez.Control5B.Model.DAO.GolesDAO;
import cl.peivandPerez.Control5B.Model.DTO.GolesDTO;

@Service
public class GolesServiceImpl implements GolesService {

	@Autowired
	GolesDAO golesDAO;
	
	@Override
	public List<GolesDTO> listAll() {
		return golesDAO.listAll();
	}

	@Override
	public List<GolesDTO> getGolesEquipo() {
		return golesDAO.getGolesEquipo();
	}

	@Override
	public int update(GolesDTO golesDTO) {
		return golesDAO.update(golesDTO);
	}

}
